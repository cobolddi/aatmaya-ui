$(document).ready(function() {

      var scrollTop = $(".scrollTop");

      $(window).scroll(function() {
        // declare variable
        var topPos = $(this).scrollTop();

        // if user scrolls down - show scroll to top button
        if (topPos > 100) {
          $(scrollTop).css("opacity", "1");

        } else {
          $(scrollTop).css("opacity", "0");
        }

      }); // scroll END

      //Click event to scroll to top
      $(scrollTop).click(function() {
        $('html, body').animate({
          scrollTop: 0
        }, 800);
        return false;

      });

    // My Account Vertical Tabs
    $('#vertical_tab_nav > ul > li > a').eq(0).addClass( "selected" );
    $('#vertical_tab_nav > div > article').eq(0).css('display','block');


    //---------- This assigns an onclick event to each tab link("a" tag) and passes a parameter to the showHideTab() function
            
    $('#vertical_tab_nav > ul').click(function(e){
        
        if($(e.target).is("a")){

            /*Handle Tab Nav*/
            $('#vertical_tab_nav > ul > li > a').removeClass( "selected");
            $(e.target).addClass( "selected");

            /*Handles Tab Content*/
            var clicked_index = $("a",this).index(e.target);
            $('#vertical_tab_nav > div > article').css('display','none');
            $('#vertical_tab_nav > div > article').eq(clicked_index).fadeIn();

        }
  
        $(this).blur();
        return false;
  
    });


    // Giftcards Dropdown
    $(".GiftLeftImgRightContent .RightContent #FirstGiftCard").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="FirstGiftcard1"){
                $(".BottomContent1").not(".FirstGiftcard1").hide();
                $(".FirstGiftcard1").show();
            }
            else if($(this).attr("value")=="FirstGiftcard2"){
                $(".BottomContent1").not(".FirstGiftcard2").hide();
                $(".FirstGiftcard2").show();
            }
            else if($(this).attr("value")=="FirstGiftcard3"){
                $(".BottomContent1").not(".FirstGiftcard3").hide();
                $(".FirstGiftcard3").show();
            }
            else if($(this).attr("value")=="FirstGiftcard4"){
                $(".BottomContent1").not(".FirstGiftcard4").hide();
                $(".FirstGiftcard4").show();
            }
            else{
                $(".BottomContent1").hide();
            }
        });
    }).change();

    $(".GiftRightImgLeftContent .RightContent #SecondGiftCard").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="Giftcard1"){
                $(".BottomContent").not(".Giftcard1").hide();
                $(".Giftcard1").show();
            }
            else if($(this).attr("value")=="Giftcard2"){
                $(".BottomContent").not(".Giftcard2").hide();
                $(".Giftcard2").show();
            }
            else if($(this).attr("value")=="Giftcard3"){
                $(".BottomContent").not(".Giftcard3").hide();
                $(".Giftcard3").show();
            }
            else if($(this).attr("value")=="Giftcard4"){
                $(".BottomContent").not(".Giftcard4").hide();
                $(".Giftcard4").show();
            }
            else{
                $(".BottomContent").hide();
            }
        });
    }).change();

    $('a.dismiss').on('click', function(e) {
        $('body').addClass('close');
        e.stopPropagation();
        e.preventDefault();
    });

    $('.NavigationBlock ul li.dropdown a').on('click', function(e) {
      $('header').toggleClass('OpenDropdown');
      e.stopPropagation();
      e.preventDefault();
  });

    // Header Space
    Headspace(document.querySelector('header'))


    // Menu Overlay Animation
      var toggles = document.querySelectorAll(".c-hamburger");

      for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
      };

      function toggleHandler(toggle) {
        toggle.addEventListener("click", function(e) {
          e.preventDefault();
          if (this.classList.contains("is-active") === true) {
            this.classList.remove("is-active");
            $('.open').removeClass('oppenned');
          } else {
            this.classList.add("is-active");
            $(".open").addClass('oppenned');
          }
        });
      }
      $(".sub-menu li a").click(function(event) {
        $(".open").removeClass('oppenned');
        $(".c-hamburger").removeClass('is-active');
      });

      $(".sub-menu a.CloseBtn").click(function(event) {
        $(".open").removeClass('oppenned');
        $(".c-hamburger").removeClass('is-active');
        event.preventDefault();
      });

      // Hamburger Menu
      $(document).on('click','button.c-hamburger',function(){
        $('body').toggleClass('OverflowHidden');
      e.stopPropagation();
    });

    $(".MobileMenu ul li.menu-item-has-children").click(function(event){
      $('.MobileMenu').toggleClass('DropdownOpen');
      e.stopPropagation();
      e.preventDefault();
    });

    $(".dropdown").hover(
        function() { $('.dropdown-menu', this).fadeIn(300);
        },
        function() { $('.dropdown-menu', this).fadeOut(300);
    });

    $(".ShoppingOptions ul li.whistlistIcon a").click(function(event) {
        $(".ShoppingOptions ul li.whistlistIcon").toggleClass('filled');
      });

    $(".TopImgBtmTextCard .ProductWithOptions .OptionList ul li.whistlistIcon").click(function(event) {
        // $(".TopImgBtmTextCard .ProductWithOptions .OptionList ul li.whistlistIcon").toggleClass('filled');
        $(this).toggleClass("filled");
      });

    $(".SortingProductLists .ShowingProducts .OptionList ul li.whistlistIcon").click(function(event) {
        // $(".TopImgBtmTextCard .ProductWithOptions .OptionList ul li.whistlistIcon").toggleClass('filled');
        $(this).toggleClass("filled");
      });

      $(".GiftLeftImgRightContent .RightContent .BaseBorderButton").click(function(event) {
        // $(".TopImgBtmTextCard .ProductWithOptions .OptionList ul li.whistlistIcon").toggleClass('filled');
        $(this).toggleClass("filled");
      });

      $(".SingleProductBlock .SingleProductContent .BaseBorderButton").click(function(event) {
        // $(".TopImgBtmTextCard .ProductWithOptions .OptionList ul li.whistlistIcon").toggleClass('filled');
        $(this).toggleClass("filled");
      });

    $('.RelatedProduct').slick({
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: true,
      responsive: [{
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 400,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
      }]
  });
	
    $('.slider').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        adaptiveHeight: true,
        autoplaySpeed: 2000,
        arrows: true,
        fade: false,
        responsive: [{
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 400,
           settings: {
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });


    
    

    $('.image-popup-vertical-fit').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom', 
        gallery:{
            enabled:true
          },
      
        zoom: {
          enabled: true, 
      
          duration: 300, // duration of the effect, in milliseconds
          easing: 'ease-in-out', // CSS transition easing function
      
          opener: function(openerElement) {
      
            return openerElement.is('img') ? openerElement : openerElement.find('img');
        }
      }
    
    });

    // Login Popup Modal

    $('.login-popup-link').magnificPopup({
      type: 'inline',
      midClick: true,
      mainClass: 'mfp-fade'
    });


    // Sorting Products  

    var $grid = $('.grid').isotope({
      itemSelector: '.element-item',
      layoutMode: 'fitRows'
    });
    // filter functions
    var filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function() {
        var number = $(this).find('.number').text();
        return parseInt( number, 10 ) > 20;
      },
      // show if name ends with -ium
      ium: function() {
        var name = $(this).find('.name').text();
        return name.match( /ium$/ );
      }
    };
    // bind filter on select change
    $('.filters-select').on( 'change', function() {
      // get filter value from option value
      var filterValue = this.value;
      // use filterFn if matches value
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });


    $(function(){
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            autoplay: true
        });
        $('.slider-nav').slick({
            autoplay: true,
            vertical: true,
            infinite: true,
            verticalSwiping: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: true,
            arrows: true,
            focusOnSelect: true,
            responsive: [{
              breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    vertical: false,
                }
            }]
        });    
    });



})

