<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/paheheader/NoBanner.php') ?>

<section class="Breadcrum">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Shopping Cart</a></li>
            <li>Checkout</li>
        </ul>
    </div>
</section>


<?php @include('template-parts/CheckoutBlock.php') ?>


<?php @include('template-parts/footer.php') ?>