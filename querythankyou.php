<?php @include('template-parts/header.php') ?>

<section class="ThankyouBanner">
    <img src="assets/img/bannerbg.png" alt="" class="BannerImg">
    <div class="BannerContent">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="Bannertext">
                        <h1 class="WhiteText">Thank you</h1>
                        <p class="WhiteText">For your query. We will get in touch with you very soon.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <img src="assets/img/querybanner.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>