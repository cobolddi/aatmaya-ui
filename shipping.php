<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Section ShippingReturnBlock">
    <div class="container">
        <h2 class="TextCenter BaseColorText">shipping and returns</h2>
        <h5>Shipping</h5>
        <p>All orders are shipped from Vancouver, Canada. Orders are processed and dispatched within 5 working days of being placed. Due to current Covid-19 restrictions, there may be delays in delivery times depending on respective country locations.</p>
        <h5>Customs and Duty</h5>
        <p>International orders may be subject to customs and duty once they reach the destination country. Depending on the custom and import laws of each country, orders may be applicable to additional charges which should be taken care of by the recipient. Aatmaya will not be held liable for any loss of funds or additional costs incurred for dispatched orders.</p>
        <h5>Returns and Exchanges</h5>
        <p>Refunds and exchanges are applicable for undamaged and unused products that are returned within 30 days of purchase. Shipping charges will not be refunded. Return of products are at the cost of the recipient and should include a return label if an exchange is to be processed. If a return label is not provided, the return postage or shipping charges paid by Aatmaya will be deducted from the refund provided.</p>
        <p>Refunds and exchanges are applicable for items that arrive to the recipient in faulty or damaged conditions. A full refund including shipping charges will be provided. If exchanges are requested, shipping charges should be borne by the recipient when retuning the defected items. In this case, no return label is required as Aatmaya will bear the shipping charges when providing the new item to the recipient.</p>
        <p>Return of products must be in their original condition and packaging. For returns on jewellery, please include all items in a bubble-padded envelope to prevent any damage during postage.</p>
        <p>For refunds and exchanges, please email <a href="mailto:crystalwarriors@aatmaya.com">crystalwarriors@aatmaya.com</a> with proof of purchase. You will receive an email within 48 hours with instructions on returning your product.</p>
        <p>Refunds and exchanges are not applicable on custom orders.</p>
        <p>Due to maintaining suitable health standards, refunds and exchanges are not applicable on earring purchases.</p>
        <p>By completing a purchase through the Aatmaya online shop, you agree to all the terms and conditions listed above.</p>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>