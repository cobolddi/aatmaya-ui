<?php @include('template-parts/header.php') ?>

<section class="ThankyouBanner">
    <img src="assets/img/bannerbg.png" alt="" class="BannerImg">
    <div class="BannerContent">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="Bannertext">
                        <h1 class="WhiteText">Congratulations</h1>
                        <p class="WhiteText">On choosing to uplift and transcend your journey with Shiri Perera.<br> You will receive an email with dates, preparations & the invoice for your booking. <br>Thank you.</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <img src="assets/img/tempimg/congrats.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section OrderBillingDetails">
    <div class="container">
        <div class="BriefDeatils">
            <ul>
                <li>
                    <p>Order Number</p>
                    <h5>740</h5>
                </li>
                <li>
                    <p>Date:</p>
                    <h5>Date November 25, 2020</h5>
                </li>
                <li>
                    <p>Email:</p>
                    <h5>ankur@cobold.in</h5>
                </li>
                <li>
                    <p>Total:</p>
                    <h5>$ 1,080.00</h5>
                </li>
                <li>
                    <p>Payment Method:</p>
                    <h5>Direct bank transfer</h5>
                </li>
            </ul>
        </div>
        <div class="OrderDetails">
            <h4 class="BaseColorText">Order details</h4>
            <ul>
                <li>
                    <div class="LeftText">
                        <p>Product</p>
                    </div>
                    <div class="RightText">
                        <p>Total</p>
                    </div>
                </li>
                <li>
                    <div class="LeftText">
                        <p>6 Session Package ($1080) × 1</p>
                    </div>
                    <div class="RightText">
                        <p>$1,080.00</p>
                    </div>
                </li>
                <li>
                    <div class="LeftText">
                        <p><span>Appointment Info:</span></p>
                        <p><span>Local Time: </span>October 22, 2020 10:30 am</p>
                        <p><span>Client Time: </span>(UTC+05:30)October 22, 2020 4:00 pm</p>
                        <p><span>Service: </span>6 Session Package ($1080)</p>
                        <p><span>Total Number of Persons: 1</span></p>
                    </div>
                    <!-- <div class="RightText">
                        <p>Total</p>
                    </div> -->
                </li>
                <li>
                    <div class="LeftText">
                        <p>Subtotal</p>
                    </div>
                    <div class="RightText">
                        <p>$1,080.00</p>
                    </div>
                </li>
                <li>
                    <div class="LeftText">
                        <p>Payment method:</p>
                    </div>
                    <div class="RightText">
                        <p>Direct bank transfer</p>
                    </div>
                </li>
                <li>
                    <div class="LeftText">
                        <p><span>Total</span></p>
                    </div>
                    <div class="RightText">
                        <p><span>$1,080.00</span></p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="BillingAddress">
            <h4 class="BaseColorText">Billing Address</h4>
            <p>Cobold Digital</p>
            <p>Delhi 110017 India</p>
            <p><a href="tel:9876543210">9876543210</a></p>
            <p><a href="mailto:ankur @cobold .in">ankur @cobold .in</a></p>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>