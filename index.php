<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/HomeBanner.php') ?>

<?php @include('template-parts/GreyBgImgWithContent.php')?>

<section class="Section TopImageBottomTextProduct">
    <div class="container">
        <h2>Crystal Jewellery</h2>
        <p class="Content">Made with love, grace and intention. A collection of signature pieces that are powerful to the touch, attractive to the eye, and healing for the soul.</p>
        <div class="TopImgBtmTextCard">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="SingleProduct.php">
                            <div class="ProductCard">
                                <img src="assets/img/products/Veritas.jpg" alt="">
                                <h5>Veritas <br><span>$ 162.06</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Freya.jpg" alt="">
                                <h5>Freya <br><span>$ 85.44</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Sarah.jpg" alt="">
                                <h5>Sarah <br><span>$ 67.52</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Michale.jpg" alt="">
                                <h5>Michael <br><span>$ 68.71</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/TranQuillitas.jpg" alt="">
                                <h5>Tranquilitas <br><span>$ 115.96</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Sekhmet.jpg" alt="">
                                <h5>Sekhmet <br><span>$ 84.25</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Uriel.jpg" alt="">
                                <h5>Uriel <br><span>$ 112.78</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Atlantis.jpg" alt="">
                                <h5>Atlantis <br><span>$ 68.71</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ViewAllProductsLink">
            <a href="ShopListing.php" class="NormalBaseLink">View all</a>
        </div>
    </div>
</section>

<section class="Section OffWhiteSection TestinomialSliders">
    <div class="container">
        <div class="slider">
            <div class="SliderContent">
                <h4>I would honestly recommend everything about Aatmaya; great sense of grace and style, the intricate and elaborate work behind the pendants, how all crystals are carefully chosen and all the deep thought and care that are poured into the work! I am in love with my Turquoise and Amazonite pendant, the green color compliments the golden chain so beautifully and no matter how much I LOVE to wear it on the daily, I usually let it decide when it wants to be worn; it normally happens on days when I feel exceptionally fiery.</h4>
                <h5>Rana Accawi<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased a sublime amethyst ring and a beautiful rose quartz and smokey quartz pendant that I immediately fell in love with as soon as I saw them. What I particularly admire about them is that the gemstones are big with the best quality possible and the design is extremely delicate. I love wearing Aatmaya and having it in my jewelry collection.</h4>
                <h5>Houda Henniche<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased the Moonstone Pendant from Aatmaya today and I must say, it is a beautiful piece of jewellery. Elegant and tastefully crafted, it is sure going to be my go to necklace and pendant. Wonderful work and splendid customer service. I look forward to doing more business with you.</h4>
                <h5>Shazna Hafeez Careem<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I think mere words are not enough to describe the Crystal Warrior, Shiri Perera, behind Aatmaya. Not only does she dedicate her time to travel the world and find the most beautiful gems, she also inspires healing and love and that is truly felt through her work and highpieces. I love everything about Aatmaya, the passion, love and determination are all qualities that are absolutely inspiring.</h4>
                <h5>Tracy Khoueiry<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I love Aatmaya starting from the founder! Her energy is expressed through her crystals. humanity, transcendence, spiritual consciousness, love, unity, universe, and one-being! thank you Aatmaya for you!</h4>
                <h5>Eva Michelle-Naim<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Thank you Aatmaya for genuinely caring and taking the time to help me pick the right crystal. In love with my Malachite pendant and ready for abundance and transformation!</h4>
                <h5>Nathalie El Jorr<br><span>Lebanon</span></h5>
            </div>
        </div>
    </div>
</section>

<section class="left_image_right_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
              <div class="imgWrap">
                  <img src="assets/img/tempimg/gift.png" alt="gift_image">
              </div>
            </div>
            <div class="col-md-6">
                <div class="contentWrap mr-5">
                    <h2>Gift</h2>
                    <p>Present your loved ones with the gift of healing. Choose a gift card for a special occasion or just to let them know that you care. Perfect for conscious relationships anchored on spiritual growth.</p>
                    <a href="gift.php">Read more</a>
                </div>
            </div>

            <div class="col-md-6 MobileOnly">
              <div class="imgWrap">
                  <img src="assets/img/tempimg/heal.png" alt="gift_image">
              </div>
            </div>

            <div class="col-md-6">
                <div class="contentWrap text-left-md">
                    <h2>Heal</h2>
                    <p>Embrace your spiritual awakening with a deeply immersive and personal crystal healing session with Shiri Perera. A certified crystal healer and channel, Shiri holds space for healing and transformation in your life.</p>
                    <a href="heal.php">Read more</a>
                </div>
            </div>

            <div class="col-md-6 DesktopOnly">
              <div class="imgWrap">
                  <img src="assets/img/tempimg/heal.png" alt="gift_image">
              </div>
            </div>
        </div>
    </div>
</section>

<section class="Section TopImageBottomTextProduct">
    <div class="container">
        <h2>Temple Jewelry</h2>
        <p class="Content">A tribute to the tribes of South East Asia. A curated collection of traditional jewellery to awaken your tribal power and invoke the sacred within.</p>
        <div class="TopImgBtmTextCard">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="SingleProduct.php">
                            <div class="ProductCard">
                                <img src="assets/img/products/Cassandra.jpg" alt="">
                                <h5>Cassandra <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Elisa.jpg" alt="">
                                <h5>Elisa <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Gardenia.jpg" alt="">
                                <h5>Gardenia <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Isabella.jpg" alt="">
                                <h5>Isabella <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Nora.jpg" alt="">
                                <h5>Nora <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Raven.jpg" alt="">
                                <h5>Raven <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Yasmine.jpg" alt="">
                                <h5>Yasmine <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Zara.jpg" alt="">
                                <h5>Zara <br><span>$ 15</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ViewAllProductsLink">
            <a href="ShopListing.php" class="NormalBaseLink">View all</a>
        </div>
    </div>
</section>

<?php @include('template-parts/MeetTheArtist.php')?>

<?php @include('template-parts/FollowCrystalGallery.php')?>

<?php @include('template-parts/footer.php') ?>
