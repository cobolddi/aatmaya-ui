<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Breadcrum">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Product Category</a></li>
            <li>Product Details</li>
        </ul>
    </div>
</section>
    
<section class="Section SingleProductBlock">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="ProductSlider">

                    <div class="DesktopOnly">
                        <div class="slider-nav">
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-2.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-3.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-4.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="slider-for">
                        <div class="item">
                            <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                        </div>
                        <div class="item">
                            <img src="assets/img/singleproducts/Veritas-2.jpg" alt="image" draggable="false"/>
                        </div>
                        <div class="item">
                            <img src="assets/img/singleproducts/Veritas-3.jpg" alt="image" draggable="false"/>
                        </div>
                        <div class="item">
                            <img src="assets/img/singleproducts/Veritas-4.jpg" alt="image" draggable="false"/>
                        </div>
                        <div class="item">
                            <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                        </div>
                    </div>

                    <div class="MobileOnly">
                        <div class="slider-nav">
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-2.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-3.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-4.jpg" alt="image" draggable="false"/>
                            </div>
                            <div class="item">
                                <img src="assets/img/singleproducts/Veritas-1.jpg" alt="image"  draggable="false"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="SingleProductContent">
                    <h2>Veritas</h2>
                    <h3>$ 162.06</h3>
                    <ul>
                        <li>Product Type: Pendants</li>
                        <li>Name of Crystal(s): Moonstone, Turquoise</li>
                        <li>Dimensions: 4.2 x 1 in</li>
                        <li>Weight: 10 g</li>
                    </ul>
                    <p>Free 30-minute private crystal healing session for purchases above $200</p>
                    <div class="ButtonsBlock">
                        <div class="Quantity">
                            <div class="Border">
                                <a href="#">-</a>
                                <span>1</span>
                                <a href="#">+</a>
                            </div>
                        </div>
                        <a href="" class="Addtocartbtn">Add to cart <img src="assets/img/white-cart.svg" alt=""></a>
                        <!-- <a href="" class="BaseBorderButton">Add to Wishlist <img src="assets/img/wishlish-theme.svg" alt=""></a> -->
                        <a class="BaseBorderButton">Add to Wishlist 
                            <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                            <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="ProductDescription">
                    <p>Moonstone is a crystal of initiation, drawing energy from the moon and directing them towards vibrations of new beginnings, phases and experiences in life, this crystal heals your inner child and prepares you for periods of transformation. It is also a powerful crystal for self-awareness, psychic gifts, and communication with Higher Beings, allowing for a flow of guidance from your spiritual guides. Turquoise is a crystal used for activating and healing one’s throat chakra. By enhancing communication and encouraging self-expression which is strongly associated with the goddess of truth, Veritas, turquoise assists the wearer to speak up without fear or judgement and communicate authenticity rather than ego. By practicing honesty and authenticity every day is a form of raising your vibration, and thus frequency, allowing your energy to be attuned with that of the Universe, and to be attuned with the frequency of love. The combination of moonstone and turquoise creates a powerful amulet of attracting honesty, improved communication, and an unlimited flow of abundance.</p>
                    <p>This signature piece comes with a 24-inch gold plated cable chain.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section RelatedProductSlider OffWhiteSection">
    <div class="container">
        <h2 class="TextCenter BaseColorText">You may also like</h2>
        <div class="RelatedProduct">
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
        </div>
        
    </div>
</section>

<?php @include('template-parts/footer.php') ?>

