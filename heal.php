<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/HealOverflowBanner.php') ?>

<section class="Section ContainerLeftImgRightContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="LeftImg">
                    <img src="assets/img/tempimg/heal-img1.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <p>Crystals are high-vibrational beings from the first plane of existence coded with powerful metaphysical properties. Since ancient times, several cultures and tribes have embraced and utilized crystals for attracting wealth and prosperity, forming protective shields, welcoming love and abundance, initiating manifestation and even as an alternative to traditional medicine.</p>
                    <p> While the ultimate power to heal ourselves lies within, crystal healing is a powerful method that helps reconstruct limiting beliefs, amplify positive vibrations and align to the Universal Energy Matrix.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="BaseColorBg PrivateSessionsSection">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <img src="assets/img/tempimg/heal-sessions.png" alt="">
            </div>
            <div class="col-12 col-md-6">
                <div class="ContentBlock">
                    <h2 class="WhiteText">private sessions</h2>
                    <p class="WhiteText">Choose an immersive session where Shiri creates a personalized grid to liberate you from negative energies and welcome positive vibrations into your life.</p>
                    <a href="sessions.php" class="NormalWhiteLink">Book a session</a>
                </div>
            </div>
        </div>
    </div>
    <div class="FloatingImg DesktopOnly" style="background: url(assets/img/tempimg/heal-sessions.png) 0 0 no-repeat;">
        
    </div>
</section>

<section class="Section ContainerLeftImgRightContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="LeftImg">
                    <img src="assets/img/tempimg/heal-event.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <h2 class="BaseColorText">events</h2>
                    <p>Be a part of a deeper connection and explore group sessions that will transcend you into a higher understanding of oneness and collective healing.</p>
                    <a class="NormalBaseLink">Coming soon</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section TestinomialSliders OffWhiteSection">
    <div class="container">
        <div class="slider">
            <div class="SliderContent">
                <h4>I had such a beautiful experience. I found myself very relaxed, comfortable and calm, and I could feel her presence and support. I had been a bit out of my spiritual practices/meditations, and not only was I guided through meditations provided, I also had my downloaded affirmations and recommended crystals to work with, I feel as though I found peace and love within again. I was guided to continue my practice and felt great value from my time working with this beautiful woman. I look forward to a session again in the future, and highly recommend anyone who has an open mind and heart to invest in yourself with these wonderful offerings.</h4>
                <h5>Alysia Pinter<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>My meditation with Shiri was other worldly. I've tried meditation in the past, using apps or videos to guide me but always felt lonely in the process. Meeting with Shiri, although through Zoom it still felt so personal, I found I was able to truly feel the benefits of meditation. Using crystals to activate sensations and awareness, it was the first meditation I felt really worked for me. I can't thank Shiri enough for this experience. I've been going through many personal changes and started applying the recommended meditation practices which have helped tremendously. I feel stronger and more able to make the changes I need to better my mind and body.</h4>
                <h5>Bekki Lana<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>My crystal session was truly an amazing experience. It was unbelievable how I felt I entered another dimension. Everything, from my personalised crystal grid to the guided meditation was on point, like you have known me my whole life. I can’t wait to continue on my spiritual journey and grow with your help.</h4>
                <h5>Araceli Galvez<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I am glad I did my session with Shiri. It gave me clarity and helped me focus on myself. With her guidance, I felt more powerful and was able to experience the benefits of the process even more. I am grateful to have crossed paths with Shiri and wear her jewellery which I absolutely love. I feel as if they were created just for me! I am certain that I will always come back to Shiri for more information and guidance on which crystals will continue to help me with my alignment and I encourage others to do the same.</h4>
                <h5>Grace Khoury <br><span>Dubai</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Shiri, a powerful woman and without a doubt, a healer. My crystal healing session was an investment to understanding the shift that needed to be taken in order to help me become a stronger, and more balanced person. Such a beautiful experience in spiritual and emotional healing.</h4>
                <h5>Ashani De Silva<br><span>Sri Lanka</span></h5>
            </div>
            <div class="SliderContent">
                <h4>One benefit I felt from having the session was that my intuition about my patterns and current situation was confirmed. Shiri was able to tap into seemingly unrelated areas of my life that were in fact deeply connected to my emotions. Also, I think the fact that Shiri uses stones that she herself harvested is a huge benefit to the session. Thank you Aatmaya so much for the session. It has shifted a great deal of patterns within me, and I truly feel supported and energized by it. You truly have a gift here. Thank you for sharing it with me. I look forward to our next session.</h4>
                <h5>Layla Habib<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Shiri is a being of light and I felt protected and comforted as I was gently guided through a beautiful energy session with her. She was able to feel my energy from miles away and she gave me a wonderful aura reading, filled with empathy and guidance. She had even constructed a special meditation and crystal grid just for me which I apply until this very day. It was a magical experience.</h4>
                <h5>Rana Accawi<br><span>Lebanon</span></h5>
            </div>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>