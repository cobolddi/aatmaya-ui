<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Section ShippingReturnBlock">
    <div class="container">
        <h2 class="TextCenter BaseColorText">term and conditions</h2>
        <h5>Overview</h5>
        <p>The purpose is to create, uplift and transcend, and believe that the customer service should be of the same vibration. We only hope that through exploring Aatmaya and what we have to offer, we will leave you feeling loved, comforted and protected through our products and services. By placing an order or booking a service with Aatmaya, you will be deemed to have read, understood and agreed to these Terms and Conditions.</p>
        <p>This website is operated by Aatmaya. As you navigate through our website, the terms “we”, “us”, and “our” refer to Aatmaya. Aatmaya offers this website, including all information, tools and services available from our site to you, the user, is conditioned upon your acceptance of all terms, conditions, policies and notices stated here.</p>
        <p>Upon visiting our site and/or purchasing a product or service, you engage in our service and agree to be bound by the following terms and conditions which apply to all users of our site including, without limitation, users who appear as browsers, vendors, customers, merchants and/or contributors of content.</p>
        <p>Please read our Terms and Conditions carefully before accessing our website.</p>
        <p>The Aatmaya online store is hosted on Wordpress Inc. They provide us with the online e-commerce platform that allows us to sell our products and provide services to you.</p>

        <h5>Section 1 - Online Store Terms</h5>
        <p>A contract between the customer and/or client and Aatmaya for the sale of our products will only exist once an order has been accepted, processed and dispatched. The sale of our services will only exist once a booking has been accepted, processed and conducted. This does not affect the customer’s statutory rights.</p>
        <p>Prices, product descriptions and availability of goods are subject to change without notice.</p>
        <p>Products will be shipped in 4 business days and delivered within 7 working days for orders to Canada/USA, and between 10-14 working days to other countries. All quoted delivery time periods are estimated and may be subject to change. We will not be held liable for any loss or damage incurred due to delayed delivery times or mishandling of the products during the delivery process once they have left their origin.</p>
        <p>Individual Crystal Healing sessions are booked via our website. For changes in booking dates and times, please contact us via <a href="mailto:crystalwarriors@aatmaya.com">crystalwarriors@aatmaya.com</a> following which a response will be provided within 48 hours.</p>

        <h5>Section 2 - General Conditions</h5>
        <p>We reserve the right to refuse service to anyone for any reason at any time.</p>
        <p>You agree to not reproduce, duplicate, copy, sell, resell or exploit any of our products and/or services without written permission from us.</p>

        <h5>Section 3 - Products and Services</h5>
        <p>We guarantee that the colours in our product images reflect the actual colours of the physical jewelry as accurately as possible. </p>
        <p>We reserve the right, but are not obligated, to limit the sales of our products or services to any person, geographic region or jurisdiction. We may exercise this right on a case by case basis. </p>
        <p>We reserve the right to limit the quantities of any products or services that we offer.</p> 
        <p>We reserve the right to discontinue any product or service in time. </p>
        <p>We do not guarantee that the quality of any products, services, information or other material purchased by you will meet your expectations or that any errors in our products and services will be corrected.</p>

        <p><i>Custom Orders:</i></p>
        <p>Custom orders will take 4 weeks to design, create and ship. We understand this may appear long but this is only to ensure that we achieve the best possible creations according to your preferences.</p>
        <p><i>Repairs:</i></p>
        <p>Repairs are free and valid for a lifetime. Shipping costs, both ways, have to be borne by the customer.</p>
        <p><i>Materials:</i></p>
        <p>All crystal jewelry pieces are unique in design, intention and energy activation although you may expect variations in colors, sizes and shapes of crystals in custom orders. We use high quality copper wire which tightly secures and keeps the crystals intact, and a protective coating is applied on each piece as an overall finish. </p>
        <p><i>Customs and Import Taxes:</i></p>
        <p>Customers are responsible for any taxes that they might occur when purchasing any products. Aatmaya is not responsible for any postal delays due to customs or other factors.</p>

        <h5>Section 4 - Billing and Account Information</h5>
        <p>We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, household or order. These restrictions include orders placed under the same customer account, credit card, and orders that use the same billing and/or shipping address. In the event that we cancel an order, an email will be sent to the email address provided at the time of purchase. </p>
        <p>We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distribution.</p>
        <p>When providing your billing and account details, you agree to provide current, complete and accurate information for all purchases made at Aatmaya. You agree to update your account and billing information when necessary in order to help us successfully complete your transactions and contact you as needed.</p>
        <p>Any submission of personal information through our online store is governed by our <a href="privacypolicy.php">Privacy Policy.</a></p>

        <h5>Section 5 - Comments and Feedback</h5>
        <p>We are under no obligation to (1) maintain any comments in confidence, (2) pay compensation for any comments/feedback, or (3) respond to any comments.</p>
        <p>We have the right to monitor, edit or remove any content that we determine in our sole discretion to be unlawful, racist, offensive, threatening, derogative, defamatory, pornographic, obscene or otherwise questionable that violates our Terms of Service.</p>

        <h5>Section 6 -  Crystals</h5>
        <p>Our crystals hold powerful healing properties and assist all seekers in their path of healing and transformation however they are not meant to be replace any medical treatment. They may be used in addition to medical treatment when the case, and a doctor or qualified medical professional must be involved if you experience any symptoms of illness.</p>

        <h5>Section 7 - All Rights Reserved</h5>
        <p>Aatmaya holds rights to the content of all pages, including images, designs, logos written text and other materials. ALL RIGHTS RESERVED. The copying, modification, distribution, reproduction or incorporation of any material available on our site is prohibited.</p>

        <h5>Section 8 - Governing Law</h5>
        <p>These Terms and Conditions and any separate agreement whereby we provide you products or services shall be governed by and construed in accordance with the laws of British Columbia, Canada.</p>

        <h5>Section 9 - Changes to Terms and Conditions</h5>
        <p>You may review the most recent version of our Terms and Conditions at any time at this page.</p>
        <p>We reserve the right to update, change or replace any section of our Terms and Conditions. Your continued use of our website and our services following any update to our Terms and Conditions constitutes agreement with those changes.</p>

        <h5>Section 10 - Enquiries</h5>
        <p>Any enquiries regarding our Terms and Conditions can be submitted to <a href="mailto:info@aatmaya.com">info@aatmaya.com</a> </p>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>