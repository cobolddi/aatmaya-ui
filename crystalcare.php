<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Section ShippingReturnBlock">
    <div class="container">
        <h2 class="TextCenter BaseColorText">crystal care</h2>
        <p>1.Aatmaya crystal jewellery is handmade with intent, love and purpose. Each crystal is specifically chosen, an intention is made and a design is created to give you unique, one of a kind signature jewellery pieces that will call out to your soul and form a connection your your spirit. Each finished piece has been energetically activated by certified Crystal Healer, Shiri Perera, using a powerful blend of deep meditation, channelling, Chakra stimulation and traditional, hand-rolled incense.</p>

        <p>2. Crystals are wrapped with high-quality copper wire and finished pieces are completed with an anti-tarnish finish that is beneficial in both appearance and longevity.</p>

        <p>3. Caring for your crystals is an important, beautiful and educational experience. When you begin to wear crystals or use them to help you in your spiritual transformation, it is vital that you keep them cleansed and energised in order to fulfil their purpose and assist you in your extraordinary journey. </p>
        <p>4. It is important to us that your crystal continues to look divine and spread its light. You may use the following guide to make this process straightforward and easy so that your crystal can be well cared for.</p>
            <p>4.1 Keep your crystals from all liquids including water. As much as you would like to wear your glittering beauty to the beach, crystals have a Mohs hardness level, which is basically its brittleness, that can vary from crystal to crystal. You would not want your crystals to slowly disintegrate with time, so it is best to keep them away from all liquids as much as possible.</p>
            <p>4.2 Always remove your crystal jewellery before doing household chores or other activities of a similar nature, exercising and sleeping. Our crystal jewellery is delicate and not to be worn in strenuous activities.</p>
            <p>4.3 Avoid direct contact with beauty products such as body mists, essential oils, body butters and all other beauty care essentials when wearing your crystal jewellery.</p>
            <p>4.4 Clean your crystals gently with a soft, cleaning cloth. They attract dust particles in a blink of an eye and keep your crystals clean and shiny.</p>
            <p>4.5 All crystals are entwined in high-quality copper wire and completed with an anti-tarnish finish, therefore there is no need to resort to home-remedies of cleaning copper wire since all our finished pieces including the copper wire are sealed with a protective coating.</p>
            <p>4.6 Store your crystal jewellery in a safe and secure box and keep them separate from one another in order to avoid marks, scratches and entanglement.</p>
        <p>5. To energise your crystals, that is an entirely different process. Aatmaya recommends to energise your crystals at least one a month so that the energies are renewed and intention is re-ignited. You may use the following suggestions as a guide on how to energise your crystals.</p>
            <p>5.1 Energise your crystals at every Full Moon. Lay your crystals on a flat surface on the night of a Full Moon, say a prayer, set an intention and ask the divine energies of the blessed Full Moon to touch your crystals.</p>
            <p>5.2 Use incense. Lighting traditional, hand-rolled incense and chanting mantras is a wonderful combination to re-energise your crystals.</p>
            <p>5.3 Meditation. Through active meditation and the power of your mind and emotions, you form a special connection with your crystals as the energy that you are transmuting towards them is very personal and powerful since it comes from you, the owner. You begin to create a strong communication channel between yourself and the crystals, creating a balance and sense of harmony between the two.</p>
            <p>5.4 Sound Healing. Applying a range of Sound Healing techniques such as the use of singing bowls, tuning forks, or gongs combined with powerful intention and prayer, is also an effective way in cleansing and energising your crystals.</p>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>