<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/paheheader/NoBanner.php') ?>

<section class="Breadcrum">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li>Shopping Cart</li>
        </ul>
    </div>
</section>

<section class="ShoppingCartBlock">
    <div class="container">
        <h2 class="TextCenter BaseColorText">Shopping Cart</h2>
        <div class="DesktopOnly">
            <div class="ProductListHead">
                <div class="Product">Product</div>
                <div class="Price">Price</div>
                <div class="Quantity">Quantity</div>
                <div class="Subtotal">Subtotal</div>
                <div class="Close"></div>
            </div>
        </div>
        <ul class="CartList">
            <li>
                <div class="Product">
                    <div class="Img"><a href="#"><img src="assets/img/productthumb.png" alt=""></a></div>
                    <a href="#">
                        <div class="ProductDetails">
                            <h4>Agate Way to Hell</h4>
                            <p>Dimensions - 3.6 × 1.5 in</p>
                            <p>Weight - 20 g</p>
                        </div>
                    </a>
                </div>
                <div class="Price">
                    <div class="MobileOnly">Price</div>
                    <a href="#">$ 250/-</a>
                </div>
                <div class="Quantity">
                    <div class="MobileOnly">Quantity</div>
                    <div class="Border">
                        <a href="#">-</a>
                        <span>1</span>
                        <a href="#">+</a>
                    </div>
                </div>
                <div class="Subtotal">
                    <div class="MobileOnly">Subtotal</div>
                    $500/-
                </div>
                <div class="Close">
                    <a href="#"><img src="assets/img/close.svg" alt=""></a>
                </div>
            </li>
            <li>
                <div class="Product">
                    <div class="Img"><a href="#"><img src="assets/img/productthumb.png" alt=""></a></div>
                    <a href="#">
                        <div class="ProductDetails">
                            <h4>Agate Way to Hell</h4>
                            <p>Dimensions - 3.6 × 1.5 in</p>
                            <p>Weight - 20 g</p>
                        </div>
                    </a>
                </div>
                <div class="Price">
                    <div class="MobileOnly">Price</div>
                    <a href="#">$ 250/-</a>
                </div>
                <div class="Quantity">
                    <div class="MobileOnly">Quantity</div>
                    <div class="Border">
                        <a href="#">-</a>
                        <span>1</span>
                        <a href="#">+</a>
                    </div>
                </div>
                <div class="Subtotal">
                    <div class="MobileOnly">Subtotal</div>
                    $500/-
                </div>
                <div class="Close">
                    <a href="#"><img src="assets/img/close.svg" alt=""></a>
                </div>
            </li>
            <li>
                <div class="Product">
                    <div class="Img"><a href="#"><img src="assets/img/productthumb.png" alt=""></a></div>
                    <a href="#">
                        <div class="ProductDetails">
                            <h4>Agate Way to Hell</h4>
                            <p>Dimensions - 3.6 × 1.5 in</p>
                            <p>Weight - 20 g</p>
                        </div>
                    </a>
                </div>
                <div class="Price">
                    <div class="MobileOnly">Price</div>
                    <a href="#">$ 250/-</a>
                </div>
                <div class="Quantity">
                    <div class="MobileOnly">Quantity</div>
                    <div class="Border">
                        <a href="#">-</a>
                        <span>1</span>
                        <a href="#">+</a>
                    </div>
                </div>
                <div class="Subtotal">
                    <div class="MobileOnly">Subtotal</div>
                    $500/-
                </div>
                <div class="Close">
                    <a href="#"><img src="assets/img/close.svg" alt=""></a>
                </div>
            </li>
        </ul>
        <div class="CouponCartUpdate">
            <div class="CouponCode">
                <form action="">
                    <input type="text" placeholder="Coupon Code">
                    <input type="submit" value="Apply">
                </form>
            </div>
            <div class="UpdateCart">
                <a href="#" class="BaseBorderButton">Update Cart</a>
            </div>
        </div>
        <div class="CheckoutBlock">
            <div class="CartTotal">
                <h4>Cart Totals</h4>
                <ul>
                    <li>
                        <span>Subtotal</span>
                        <span>$1300/-</span>
                    </li>
                    <li>
                        <span>Discount</span>
                        <span>$200/-</span>
                    </li>
                    <li>
                        <span>Total</span>
                        <span>$1100/-</span>
                    </li>
                </ul>
            </div>
            <div class="CheckOutButton">
                <a href="#" class="BaseBigButton">Proceed to Checkout</a>
            </div>
        </div>
    </div>
</section>


<section class="Section RelatedProductSlider OffWhiteSection">
    <div class="container">
        <h2 class="TextCenter BaseColorText">You may also like</h2>
        <div class="RelatedProduct">
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
        </div>
        
    </div>
</section>


<?php @include('template-parts/footer.php') ?>