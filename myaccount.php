<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Breadcrum">
    <div class="container">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Shopping Cart</a></li>
            <li><a href="#">Checkout</a></li>
            <li>Account Dashboard</li>
        </ul>
    </div>
</section>

<section class="Section MyaccountBlock">
    <div class="container">
        <div id="vertical_tab_nav" class="VerticalTabs">

            <ul class="TabsLists">
                <h4>My Account</h4>
                <li class="selected">
                    <a href="index.html">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#profile"></use>
                        </svg>
                        Account Dashboard
                    </a>
                </li>
                <li>
                    <a href="index.html">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#order"></use>
                        </svg>
                        Orders
                    </a>
                </li>
                <li>
                    <a href="index.html">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#address"></use>
                        </svg>
                        Addresses
                    </a>
                </li>
                <li>
                    <a href="index.html">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#profile-order"></use>
                        </svg>
                        Account details
                    </a>
                </li>
                <li>
                    <a href="index.html">
                        <svg class="Icon">
                            <use xlink:href="assets/img/cobold-sprite.svg#logout"></use>
                        </svg>
                        Logout
                    </a>
                </li>
            </ul>

            <div class="DetailsBlock">
                <article>
                    <h3>Account Dashboard</h3>

                    <h6>Hello, Rahul Ranjan</h6>
                    <p>From your My Account Dashboard you have the ability to view a snapshot of your recent account activity and update your account information select a link below to view or edit information.</p>

                    <div class="RecentOrderList">
                        <div class="Heading">
                            <p>Recent Orders</p>
                            <a href="" class="viewall">View All</a>
                        </div>
                        <table class="Table">
                            <thead>
                                <tr>
                                  <th>Order #</th>
                                  <th>Date</th>
                                  <th>Ship to</th>
                                  <th>Order Total</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td data-label="Order">100000142098865432</td>
                                  <td data-label="Date">04/05/2020</td>
                                  <td data-label="Ship to">-</td>
                                  <td data-label="Order Total">RS. 900/-</td>
                                  <td data-label="Status">Pending</td>
                                  <td data-label="Action">
                                    <a href="">View Order</a>
                                    <a href="">Reorder</a>
                                    <a href="">Cancel</a>
                                  </td>
                                </tr>
                                <tr>
                                  <td data-label="Order">100000142</td>
                                  <td data-label="Date">04/05/2020</td>
                                  <td data-label="Ship to">-</td>
                                  <td data-label="Order Total">RS. 900/-</td>
                                  <td data-label="Status">Pending</td>
                                  <td data-label="Action">
                                    <a href="">View Order</a>
                                    <a href="">Reorder</a>
                                    <a href="">Cancel</a>
                                  </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="AccountInfoWrap">
                        <div class="AccountInfo">
                            <h4 class="head">Account Information
                                <a href="">
                                    <img src="assets/img/edit.svg" alt="">
                                </a>
                            </h4>
                            <p>Rahul Ranjan</p>
                            <p>rahul@cobold.in</p>
                            <a href="">Change Password</a>
                        </div>
                        <div class="AddressBook">
                            <h4 class="head">Address Book</h4>
                            <p>Default Billing Address </p>
                            <p>Rahul Ranjan</p>
                            <p>Farm No. 4, Second Floor, Club Drive, Ghitorni - 110030 </p>
                            <p>
                                <a href="">Edit Address</a>
                            </p>
                        </div>
                        <div class="ManageAddress">
                            <h4 class="head">Manage Addresses</h4>
                            <p>Default Default Shipping Address </p>
                            <p>Rahul Ranjan</p>
                            <p>Farm No. 4, Second Floor, Club Drive, Ghitorni - 110030 </p>
                            <p>
                                <a href="">Edit Address</a>
                            </p>
                        </div>
                    </div>

                </article>

                <article>
                    <h3>Orders</h3>

                    <div class="dashboardBody">
                        <div class="TableWrap">
                            <div class="titleTableWrap">
                                <div class="titleTableWrap_left">
                                    <h4>4 ITEMS</h4>
                                </div>
                                <div class="titleTableWrap_right">
                                    <div class="Filter">
                                        <span>Show</span>
                                        <select name="" class="FilterSelect">
                                            <option value="">10</option>
                                            <option value="">15</option>
                                            <option value="">20</option>
                                            <option value="">25</option>
                                        </select>
                                        <span>Per Page</span>
                                    </div>
                                </div>
                            </div>
                            <table class="Table">
                                <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>Date</th>
                                        <th>Ship to</th>
                                        <th>Order Total</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-label="Order">7000142</td>
                                        <td data-label="Date">17/02/2019</td>
                                        <td data-label="Ship to">-</td>
                                        <td data-label="Order Total">RS. 1200/-</td>
                                        <td data-label="Status">Pending</td>
                                        <td data-label="Action">
                                            <a href="">View Order</a>
                                            <a href="">Reorder</a>
                                            <a href="">Cancel</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-label="Order">100000143</td>
                                        <td data-label="Date">04/05/2020</td>
                                        <td data-label="Ship to">-</td>
                                        <td data-label="Order Total">RS. 900/-</td>
                                        <td data-label="Status">Pending</td>
                                        <td data-label="Action">
                                            <a href="">View Order</a>
                                            <a href="">Reorder</a>
                                            <a href="">Cancel</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-label="Order">100000144</td>
                                        <td data-label="Date">04/05/2020</td>
                                        <td data-label="Ship to">-</td>
                                        <td data-label="Order Total">RS. 900/-</td>
                                        <td data-label="Status">Pending</td>
                                        <td data-label="Action">
                                            <a href="">View Order</a>
                                            <a href="">Reorder</a>
                                            <a href="">Cancel</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td data-label="Order">100000145</td>
                                        <td data-label="Date">04/05/2020</td>
                                        <td data-label="Ship to">-</td>
                                        <td data-label="Order Total">RS. 900/-</td>
                                        <td data-label="Status">Pending</td>
                                        <td data-label="Action">
                                            <a href="">View Order</a>
                                            <a href="">Reorder</a>
                                            <a href="">Cancel</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </article>

                <article>
                    <h3>Addresses</h3>

                    <div class="dashboardBody">
                        <div class="AccountInfoWrap FlexJC_unset">
                            <div class="AccountInfo">
                                <h4 class="head">Default Billing Address </h4>
                                <p>Rahul Ranjan</p>
                                <p>Farm No. 4, Second Floor, Club Drive, Ghitorni - 110030 </p>
                                <p>
                                    <a href="">Change Billing Address</a>
                                </p>
                            </div>
                            <div class="AddressBook">
                                <h4 class="head"></h4>
                                <p>Default Default Shipping Address </p>
                                <p>Rahul Ranjan</p>
                                <p>Farm No. 4, Second Floor, Club Drive, Ghitorni - 110030 </p>
                                <p>
                                    <a href="">Change Shipping Address</a>
                                </p>
                            </div>
                        </div>
                        <div class="BtnWrap">
                            <a href="" class="Button">Add New Address</a>
                        </div>
                    </div>
                </article>

                <article>
                    <h3>Account details</h3>

                    <div class="accountDetails">
                        <form action="">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <label for="">First Name</label>
                                    <input type="text" placeholder="Enter your first name">
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="">First Name</label>
                                    <input type="text" placeholder="Enter your first name">
                                </div>
                                <div class="col-12 col-md-12">
                                    <label for="">Email Address</label>
                                    <input type="email" placeholder="Enter your email">
                                </div>
                                <div class="col-12">
                                    <p>Change Password</p>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="">Current Password</label>
                                    <input type="password" placeholder="Enter your current password">
                                </div>
                                <div class="col-12 col-md-6">
                                    <label for="">New Password</label>
                                    <input type="password" placeholder="Enter your new password">
                                </div>
                                <div class="col-12">
                                    <div class="SubmitBlock">
                                        <input type="submit" value="Save Changes" class="BaseBigButton">
                                        <a href=""><img src="assets/img/edit.svg" alt="">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </article>

                <article>
                    <h3>Logout</h3>
                    
                    <p>students consolidate their knowledge of grapheme-phoneme blends into larger units that recur in different words</p>
                </article>
                
            </div>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>