<section class="Section BookingSessionBlock">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="BookNow">
                    <h2 class="WhiteText">crystal alignment</h2>
                    <h4 class="WhiteText">Duration: <span>60 minutes</span></h4>
                    <h4 class="WhiteText">Format: <span>In person (Vancouver residents only) online for the rest of the world</span></h4>
                    <h4 class="WhiteText">Time Zone: <span>GMT-7</span></h4>
                    <h5 class="WhiteText">Price: $150</h5>
                    <a href="#" class="BaseBigButton">Book A Session</a>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="RightDescription">
                    <h4>Intention</h4>
                    <p>To assess and align one’s chakras, identify abundance blocks, and re-ignite balance and connection.</p>
                    <h4>Flow</h4>
                    <p>The session will be guided by specific crystals that stimulate each energy point and help identify imbalances. All chakras will be re-energised and programmed with healing codes and energies of the crystals. This is followed by guided meditation, downloads of personal affirmations and the reconstruction of limiting beliefs.</p>
                    <h4>Results</h4>
                    <ul>
                        <li>Insight into the exquisite world of crystals </li>
                        <li>Cleansing, re-energising and balancing of all chakras</li>
                        <li>Deep healing on an emotional and spiritual level</li>
                        <li>Wider understanding and deeper connection of the seeker’s 3-D reality with the highest plane of consciousness</li>
                    </ul>
                    <h4>Added value</h4>
                    <ul>
                        <li>Access to Shiri Perera for 30 days</li>
                        <li>Daily meditative exercises</li>
                        <li>Advice on your personal crystal to assist in daily meditations and for guidance and soul direction</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>