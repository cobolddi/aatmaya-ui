<section class="Section FollowGallery">
    <div class="container">
        <div class="GalleryBlockWithHeading">
            <h2 class="BaseColorText TextCenter">Follow the crystal</h2>
            <div class="row">
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery1.png" title="">
                            <img src="assets/img/gallery1.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery2.png" title="">
                            <img src="assets/img/gallery2.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery3.png" title="">
                            <img src="assets/img/gallery3.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery4.png" title="">
                            <img src="assets/img/gallery4.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery5.png" title="">
                            <img src="assets/img/gallery5.png" alt="" />
                        </a>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="magnific-img">
                        <a class="image-popup-vertical-fit" href="assets/img/gallery6.png" title="">
                            <img src="assets/img/gallery6.png" alt="" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>