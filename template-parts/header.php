<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Aatmaya</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
    <link href="assets/css/vendor.min.css" rel="stylesheet">
	<link href="assets/css/styles.min.css" rel="stylesheet">
	
</head>
<body>


<header>	
	<div class="DeliveryMessageBox">
		<div class="container">
			<div class="row">
				<div class="col-10 col-md-10">
					<p>DHL shipping worldwide and free shipping on all orders over $200. <a href="shipping.php">Read more</a></p>
				</div>
				<div class="col-2 col-md-2">
					<div class="Dismiss">
						<a href="#" class="dismiss"><span class="DesktopOnly">Dismiss</span> <img src="assets/img/close.svg" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<div class="LogoSocialBlock">
		<div class="container">
			<div class="row">
				<div class="col-3 col-md-4 DesktopOnly">
					<div class="SocialBlock">
						<ul>
							<li><a href="https://www.instagram.com/myaatmaya/" target="_blank"><img src="assets/img/SocialIcons/Insta.svg" alt=""></a></li>
							<li><a href="https://www.facebook.com/aatmaya" target="_blank"><img src="assets/img/SocialIcons/Facebook.svg" alt=""></a></li>
							<li><a href="https://www.youtube.com/channel/UCUXLsDrQ7rIxvokvbclYBiQ" target="_blank"><img src="assets/img/SocialIcons/Youtube.svg" alt=""></a></li>
							<li><a href="https://www.pinterest.com/aatmaya/" target="_blank"><img src="assets/img/SocialIcons/Pinterest.svg" alt=""></a></li>
						</ul>
					</div>
				</div>
				<div class="col-2 col-md-4 MobileOnly">
				<button class="c-hamburger c-hamburger--htx">
					<span></span>
				</button>
				</div>
				<div class="col-6 col-md-4">
					<div class="logoblock">
						<a href="index.php"><img src="assets/img/logo.svg" alt=""></a>
					</div>
				</div>
				<div class="col-4 col-md-4">
					<div class="ShoppingOptions">
						<ul>
							<li><a href=""><img src="assets/img/Util/Search.svg" alt=""></a></li>
							<li class="whistlistIcon">
								<a>
									<img src="assets/img/Util/Heart.svg" alt="" class="unfilled">
									<img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
								</a>
							</li>
							<li><a href="shoppingcart.php"><img src="assets/img/Util/Cart.svg" alt=""></a></li>
							<li class="DesktopOnly"><a href="#login-popup" class="login-popup-link"><img src="assets/img/Util/User.svg" alt=""></a></li>
						</ul>
					</div>
					<div id="login-popup" class="white-popup mfp-hide">
						<h2>login</h2>
						<form action="">
							<label>Email*</label>
							<input type="email">
							<label>Password</label>
							<input type="password">
							<a href="#forgetpassword" class="forget login-popup-link">Forget your password</a>
							<div class="LoginRegister">
								<input type="submit" value="Login">
								<a href="#registernow" class="login-popup-link">Register</a>
							</div>
						</form>
					</div>

					<div id="forgetpassword" class="white-popup mfp-hide">
						<h2>forget your password</h2>
						<p>Simply enter your email address below and you will receive an email with instructions on how to reset your password.</p>
						<form action="">
							<label>Email*</label>
							<input type="email">
							<div class="LoginRegister">
								<input type="submit" value="Send Link">
								<a href="#resetnow" class="login-popup-link">Reset</a>
							</div>
						</form>
					</div>

					<div id="registernow" class="white-popup mfp-hide">
						<h2>register</h2>
						<form action="">
							<div class="row">
								<div class="col-12 col-md-12">
									<label>Email*</label>
									<input type="email">
								</div>
								<div class="col-12 col-md-6">
									<label>First Name*</label>
									<input type="text">
								</div>
								<div class="col-12 col-md-6">
									<label>Last Name*</label>
									<input type="text">
								</div>
							</div>
							<div class="LoginRegister">
								<input type="submit" value="Register">
							</div>
						</form>
					</div>

					<div id="resetnow" class="white-popup mfp-hide">
						<h2>reset</h2>
						<form action="">
							<label>Create new password</label>
							<input type="password">
							<label>Re-type new password</label>
							<input type="password">
							<div class="LoginRegister">
								<input type="submit" value="Create">
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="DesktopOnly">
		<div class="NavigationBlock">
			<div class="container">
				<ul>
					<li class="dropdown"><a href="ShopListing.php">shop</a></li>
					<li><a href="gift.php">gift</a></li>
					<li><a href="heal.php">heal</a></li>
					<li><a href="aboutus.php">about</a></li>
					<li><a href="connect.php">connect</a></li>
				</ul>
			</div>
			<div class="DropdownMenu">
				<div class="container">
					<div class="MegaMenuLinks">
						<div class="row">
							<div class="col-md-3">
								<h5>Crystal Jewellery</h5>
								<ul>
									<li><a href="ShopListing.php">pendants</a></li>
									<li><a href="#">earrings</a></li>
									<li><a href="#">crystal crowns</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h5>Temple Jewellery</h5>
								<ul>
									<li><a href="#">necklaces</a></li>
									<li><a href="#">cuffs</a></li>
									<li><a href="#">pendants</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h5>Lifestyle</h5>
								<ul>
									<li><a href="#">singing bowls</a></li>
									<li><a href="#">incense</a></li>
									<li><a href="#">travelling altar kits</a></li>
								</ul>
							</div>
							<div class="col-md-3">
								<h5>Soulnets</h5>
								<!-- <ul>
									<li><a href="#">shop</a></li>
									<li><a href="#">gift</a></li>
									<li><a href="#">heal</a></li>
									<li><a href="#">about</a></li>
									<li><a href="#">connect</a></li>
								</ul> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>



<nav class="sub-menu open">
	<a href="#" class="CloseBtn">
		<span class="one"></span>
		<span class="two"></span>
	</a>

	<div class="Registerlogin">
		<a href="#registernow" class="login-popup-link">register</a><span>/</span><a href="#login-popup" class="login-popup-link">login</a>
	</div>

	<div class="MobileMenu">
		<ul>
			<li class="menu-item-has-children">
				<a href="">shop</a>
				<ul>
					<li class="category"><a href="">CRYSTAL JEWELRY</a></li>
					<li><a href="ShopListing.php">PENDANTS</a></li>
					<li><a href="">EARRINGS</a></li>
					<li><a href="">CRYSTAL CROWNS</a></li>
					<li class="category"><a href="">Temple Jewellery</a></li>
					<li><a href="#">necklaces</a></li>
					<li><a href="#">cuffs</a></li>
					<li><a href="#">pendants</a></li>
					<li class="category"><a href="">Lifestyle</a></li>
					<li><a href="#">singing bowls</a></li>
					<li><a href="#">incense</a></li>
					<li><a href="#">travelling altar kits</a></li>
					<li class="category"><a href="">Soulnets</a></li>
				</ul>
			</li>
			<li><a href="gift.php">gift</a></li>
			<li><a href="heal.php">heal</a></li>
			<li><a href="aboutus.php">about</a></li>
			<li><a href="connect.php">connect</a></li>
		</ul>
		<div class="SocialBlock">
			<ul>
				<li><a href="https://www.facebook.com/aatmaya" target="_blank"><img src="assets/img/facebook-white.svg" alt=""></a></li>
				<li><a href="https://www.pinterest.com/aatmaya/" target="_blank"><img src="assets/img/pinterest.svg" alt=""></a></li>						
				<li><a href="https://www.youtube.com/channel/UCUXLsDrQ7rIxvokvbclYBiQ" target="_blank"><img src="assets/img/youtube.svg" alt=""></a></li>
				<li><a href="https://www.instagram.com/myaatmaya/" target="_blank"><img src="assets/img/insta-white.svg" alt=""></a></li>
			</ul>
		</div>
	</div>

	
</nav>

<main>