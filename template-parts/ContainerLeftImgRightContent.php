<section class="Section ContainerLeftImgRightContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="LeftImg">
                    <img src="assets/img/purpose.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <h2 class="BaseColorText">Purpose</h2>
                    <p>To assist in the global shift by assisting seekers on their journey of self-discovery and deep healing. Aatmaya encourages grace and elegance, and is the embodiment of the deep, sacred and healing energies of the metaphysical world.</p>
                </div>
            </div>
        </div>
    </div>
</section>