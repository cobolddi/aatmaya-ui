
</main>
<footer>
	<div class="container">
		<div class="TopFooterBlock">		
			<div class="FooterMenuBlock">
				<a href="index.php" class="FooterLogo"><img src="assets/img/footer-logo.svg" alt=""></a>
				<div class="FooterNav">
					<ul>
						<li><a href="">Crystal Jewellery</a></li>
						<li><a href="">Temple Jewellery</a></li>
						<li><a href="">Lifestyle</a></li>
						<li><a href="">Soulnets</a></li>
						<li><a href="aboutus.php">About</a></li>
						<li><a href="gift.php">Gift</a></li>
						<li><a href="heal.php">Heal</a></li>
						<li><a href="connect.php">Connect</a></li>
						<li><a href="crystalcare.php">Crystal Care</a></li>
						<li><a href="shipping.php">Shipping & Returns</a></li>
						<li><a href="terms.php">Terms & Conditions</a></li>
						<li><a href="privacypolicy.php">Privacy Policy</a></li>
					</ul>
				</div>
				<div class="Newsletter">
					<h3 class="WhiteText">join the aatmaya community</h3>
					<form action="">
						<input type="email" placeholder="Email">
						<input type="submit" value="Sign up">
					</form>
					<div class="SocialBlock">
						<ul>
							<li><a href="https://www.instagram.com/myaatmaya/" target="_blank"><img src="assets/img/SocialIcons/InstaWhite.svg" alt=""></a></li>
							<li><a href="https://www.facebook.com/aatmaya" target="_blank"><img src="assets/img/SocialIcons/FacebookWhite.svg" alt=""></a></li>
							<li><a href="https://www.youtube.com/channel/UCUXLsDrQ7rIxvokvbclYBiQ" target="_blank"><img src="assets/img/SocialIcons/YoutubeWhite.svg" alt=""></a></li>
							<li><a href="https://www.pinterest.com/aatmaya/" target="_blank"><img src="assets/img/SocialIcons/PinterestWhite.svg" alt=""></a></li>						
						</ul>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div class="BottomFooterBlock">
		<div class="container">
			<div class="BtmFooterElements">
				<p>Copyright © 2020 Aatmaya.</p>
				<p>Delivered by KS Collective & <a href="https://www.cobold.in">Cobold Digital</a></p>
			</div>
		</div>
	</div>
</footer>

<div id="stop" class="scrollTop">
    <a href=""><img src="assets/img/top-arrow.svg" alt=""></a>
  </div>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>

</body>
</html>