<section class="Section GiftLeftImgRightContent GiftRightImgLeftContent">
    <div class="container">
        <h2 class="TextCenter BaseColorText">crystal healing gift</h2>
        <p class="TextCenter headpara">Let your loved one embrace an experience of crystal healing, chakra alignment and meditation.</p>
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <div class="LeftImg">
                    <img src="assets/img/crystal-healing-gift.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <form action="">
                                <select id="SecondGiftCard">
                                    <!-- <option value="Giftcard1">Gift Card 1 - $90</option>
                                    <option value="Giftcard2">Gift Card 2 - $200</option> -->
                                    <option value="Giftcard3">Gift Card 3 - $220</option>
                                    <option value="Giftcard4">Gift Card 4 - $250</option>
                                </select>
                            </form>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="Quantity">
                                <h6>Quantity</h6>
                                <div class="Border">
                                    <a href="#">-</a>
                                    <span>1</span>
                                    <a href="#">+</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-6">
                            <a href="" class="Addtocartbtn">Add to cart <span></span> $90.00 <img src="assets/img/white-cart.svg" alt=""></a>
                        </div>
                        <div class="col-6 col-md-6">
                            <a class="BaseBorderButton">Add to Wishlist 
                                <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="BottomContent Giftcard1">
                        <p>Applicable across crystal jewellery, home accessories and spiritual tools.</p>
                    </div>
                    <div class="BottomContent Giftcard2">
                        <p>Applicable across crystal jewellery, home accessories and spiritual tools. Includes a 30-minute crystal healing session worth $150.</p>
                    </div> -->
                    <div class="BottomContent Giftcard3">
                        <p>Includes a 40-minute crystal healing session worth $150.</p>
                        <p>Gift cards are delivered by email and contain instructions to redeem them at checkout. No additional processing fees.</p>
                    </div>
                    <div class="BottomContent Giftcard4">
                        <p>A 1.5-hour healing session with a personalised activated crystal grid, chakra alignment, crystal healing, meditation and crystal-infused aura protection.</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 DesktopOnly">
                <div class="LeftImg">
                    <img src="assets/img/crystal-healing-gift.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>