<section class="Section TopImageBottomTextProduct">
    <div class="container">
        <h2>Crystal Jewellery</h2>
        <p>Made with love, grace and intention. A collection of signature pieces that are powerful to the touch, attractive to the eye, and healing for the soul.</p>
        <div class="TopImgBtmTextCard">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="#"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ViewAllProductsLink">
            <a href="#" class="NormalBaseLink">View all</a>
        </div>
    </div>
</section>