<section class="ContainerRightImgLeftContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <div class="RightImg">
                    <img src="assets/img/pearls.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="LeftContent">
                    <h2 class="BaseColorText">intention</h2>
                    <p>Embrace a journey of spiritual transformation and abundance. Aatmaya helps seekers step out of the confines of their 3-D reality and into alignment with the highest plane of consciousness.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 DesktopOnly">
                <div class="RightImg">
                    <img src="assets/img/pearls.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>