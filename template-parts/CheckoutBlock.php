<section class="CheckoutSection">
    <div class="container">
        <h2 class="TextCenter BaseColorText">Checkout</h2>
        <div class="HaveCouponBlock">
            <p>Have a coupon? <a href="" class="NormalBaseLink">Click here to enter your code</a></p>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="BillingDetails">
                    <h3>Billing details</h3>
                    <form action="">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="">First Name *</label>
                                <input type="text" placeholder="Enter your first name">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Last Name *</label>
                                <input type="text" placeholder="Enter your last name">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Company name (optional)</label>
                                <input type="text" placeholder="Enter your Company name">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Country / Region *</label>
                                <select>
                                    <option value="">Country / Region *</option>
                                    <option value="">Canada</option>
                                </select>   
                            </div>
                            <div class="col-12 col-md-12">
                                <label for="">Street address *</label>
                                <input type="text" placeholder="Enter your Street address">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Apartment, suite, unit etc. (OPTL)</label>
                                <input type="text" placeholder="Enter your apartment, unit etc">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Town / City *</label>
                                <input type="text" placeholder="Enter your Town / City">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">State / County *</label>
                                <select>
                                    <option value="">British Columbia</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Postcode / ZIP *</label>
                                <input type="text" placeholder="Enter your Postcode / ZIP *">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Phone *</label>
                                <input type="text" placeholder="Enter your phone no.">
                            </div>
                            <div class="col-12 col-md-6">
                                <label for="">Email *</label>
                                <input type="email" placeholder="Enter your Email">
                            </div>
                            <div class="col-12 col-md-12">
                                <h4>Additional information</h4>
                                <label for="">Order notes (optional)</label>
                                <input type="text" placeholder="Notes about your orders">
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                    <div class="BillingDetails">
                        <h3>Your order</h3>
                        <ul class="OrderList">
                            <li><p>Agate Way to Hell X 1</p> <span>$ 500/-</span></li>
                            <li><p>Agate Way to Hell X 2</p> <span>$ 500/-</span></li>
                            <li><p>Agate Way to Hell X 3</p> <span>$ 300/-</span></li>
                        </ul>
                        <ul class="OrderList Subtotal">
                            <li><span class="themebase">Subtotal</span> <span>$1300/-</span></li>
                        </ul>
                        <ul class="OrderList Subtotal Final">
                            <li><span class="SemiBold">Subtotal</span> <span class="bold">$1300/-</span></li>
                        </ul>
                        <div class="ErrorMsgBlock">
                            <p>Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.</p>
                        </div>
                        <p class="Note">Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <a href="privacypolicy.php">privacy policy</a>.</p>
                        <div class="TickAgreement">
                            <input type="checkbox">
                            <p>I have read and agree to the website terms and conditions *</p>
                        </div>
                        <input type="submit" value="Place order" class="BaseBigButton">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>