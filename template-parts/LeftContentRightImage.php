<section class="RightImageLeftContent">
    <div class="MobileOnly">
        <div class="RightImage">
            <img src="assets/img/rightimg.png" alt="">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="LeftContent">
                    <h2 class="WhiteText">Heal</h2>
                    <p class="WhiteText">Embrace your spiritual awakening with a deeply immersive and personal crystal healing session with Shiri Perera. A certified crystal healer and channel, Shiri holds space for healing and transformation in your life.</p>
                    <a href="heal.php">Read more</a>
                </div>
            </div>
            <div class="col-12 col-md-6">
            </div>
        </div>
    </div>
    <div class="DesktopOnly">
        <div class="RightImage">
            <img src="assets/img/tempimg/heal.png" alt="">
        </div>
    </div>
</section>