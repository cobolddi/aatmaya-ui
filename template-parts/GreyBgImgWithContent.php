<section class="Section GreyBgImgWithContent">
    <div class="container">
        <div class="MidContent">
            <h4>Let your jewellery be a faithful companion on your healing journey. Welcome to Aatmaya. Handmade crystal jewellery energetically activated by artist and healer Shiri Perera. Every crystal is handpicked and woven with spiritual intention to be your perfect talisman.</h4>
        </div>
    </div>
</section>