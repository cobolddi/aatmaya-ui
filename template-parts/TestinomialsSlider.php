<section class="Section OffWhiteSection TestinomialSliders">
    <div class="container">
        <div class="slider">
    	  	<div class="SliderContent">
                <h4>I would honestly recommend everything about Aatmaya; great sense of grace and style, the intricate and elaborate work behind the pendants, how all crystals are carefully chosen and all the deep thought and care that are poured into the work! I am in love with my Turquoise and Amazonite pendant, the green color compliments the golden chain so beautifully and no matter how much I LOVE to wear it on the daily, I usually let it decide when it wants to be worn; it normally happens on days when I feel exceptionally fiery.</h4>
                <h5>Rana Accawi<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased a sublime amethyst ring and a beautiful rose quartz and smokey quartz pendant that I immediately fell in love with as soon as I saw them. What I particularly admire about them is that the gemstones are big with the best quality possible and the design is extremely delicate. I love wearing Aatmaya and having it in my jewelry collection.</h4>
                <h5>Houda Henniche<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased the Moonstone Pendant from Aatmaya today and I must say, it is a beautiful piece of jewellery. Elegant and tastefully crafted, it is sure going to be my go to necklace and pendant. Wonderful work and splendid customer service. I look forward to doing more business with you.</h4>
                <h5>Shazna Hafeez Careem<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I think mere words are not enough to describe the Crystal Warrior, Shiri Perera, behind Aatmaya. Not only does she dedicate her time to travel the world and find the most beautiful gems, she also inspires healing and love and that is truly felt through her work and highpieces. I love everything about Aatmaya, the passion, love and determination are all qualities that are absolutely inspiring.</h4>
                <h5>Tracy Khoueiry<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I love Aatmaya starting from the founder! Her energy is expressed through her crystals. humanity, transcendence, spiritual consciousness, love, unity, universe, and one-being! thank you Aatmaya for you!</h4>
                <h5>Eva Michelle-Naim<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Thank you Aatmaya for genuinely caring and taking the time to help me pick the right crystal. In love with my Malachite pendant and ready for abundance and transformation!</h4>
                <h5>Nathalie El Jorr<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I had such a beautiful experience. I found myself very relaxed, comfortable and calm, and I could feel her presence and support. I had been a bit out of my spiritual practices/meditations, and not only was I guided through meditations provided, I also had my downloaded affirmations and recommended crystals to work with, I feel as though I found peace and love within again. I was guided to continue my practice and felt great value from my time working with this beautiful woman. I look forward to a session again in the future, and highly recommend anyone who has an open mind and heart to invest in yourself with these wonderful offerings.</h4>
                <h5>Alysia Pinter<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>My meditation with Shiri was other worldly. I've tried meditation in the past, using apps or videos to guide me but always felt lonely in the process. Meeting with Shiri, although through Zoom it still felt so personal, I found I was able to truly feel the benefits of meditation. Using crystals to activate sensations and awareness, it was the first meditation I felt really worked for me. I can't thank Shiri enough for this experience. I've been going through many personal changes and started applying the recommended meditation practices which have helped tremendously. I feel stronger and more able to make the changes I need to better my mind and body.</h4>
                <h5>Bekki Lana<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>My crystal session was truly an amazing experience. It was unbelievable how I felt I entered another dimension. Everything, from my personalised crystal grid to the guided meditation was on point, like you have known me my whole life. I can’t wait to continue on my spiritual journey and grow with your help.</h4>
                <h5>Araceli Galvez<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I am glad I did my session with Shiri. It gave me clarity and helped me focus on myself. With her guidance, I felt more powerful and was able to experience the benefits of the process even more. I am grateful to have crossed paths with Shiri and wear her jewellery which I absolutely love. I feel as if they were created just for me! I am certain that I will always come back to Shiri for more information and guidance on which crystals will continue to help me with my alignment and I encourage others to do the same.</h4>
                <h5>Grace Khoury <br><span>Dubai</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Shiri, a powerful woman and without a doubt, a healer. My crystal healing session was an investment to understanding the shift that needed to be taken in order to help me become a stronger, and more balanced person. Such a beautiful experience in spiritual and emotional healing.</h4>
                <h5>Ashani De Silva<br><span>Sri Lanka</span></h5>
            </div>
    	</div>
    </div>
</section>