<section class="LeftImageRightContent">
    <div class="MobileOnly">
        <div class="LeftImage">
            <img src="assets/img/tempimg/gift.png" alt="">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">

            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <h2 class="WhiteText">Gift</h2>
                    <p class="WhiteText">Present your loved ones with the gift of healing. Choose a gift card for a special occasion or just to let them know that you care. Perfect for conscious relationships anchored on spiritual growth.</p>
                    <a href="gift.php">Read more</a>
                </div>
            </div>
        </div>
    </div>
    <div class="DesktopOnly">
        <div class="LeftImage">
            <img src="assets/img/tempimg/gift.png" alt="">
        </div>
    </div>
</section>