<section class="InsideBannerWithContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="BannerText">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut vulputate nulla. Sed ac velit urna. Curabitur in convallis leo. Vestibulum dignissim metus eros, sed faucibus nisl vulputate non.</h1>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="BannerImg">
                    <img src="assets/img/bannerimg.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>