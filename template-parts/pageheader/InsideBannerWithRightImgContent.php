<section class="InsideBannerWithRightImgContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="BannerText">
                    <h1>choose healing, choose alignment, choose love.</h1>
                    <p>Pick a gift card for your loved one and set them on a journey of<br> discovery and beauty.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="RightBannerImg">
        <img src="assets/img/giftbanner.png" alt="">
    </div>
</section>