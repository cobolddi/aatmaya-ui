<section class="HomeBanner">
    <div class="BannerContent">
        <div class="container">
            <h1>Wear your purpose</h1>
            <a href="#" class="WhiteBtn">Explore Collections</a>
        </div>
    </div>
    <!-- <img src="assets/img/banner.png" alt="" class="DesktopOnly">
    <img src="assets/img/mobile-banner.png" alt="" class="MobileOnly"> -->
    <video autoplay muted loop playsinline preload="metadata">
	  	<source src="assets/img/Aadmaya-hd.mp4" type="video/mp4">
	</video>
</section>