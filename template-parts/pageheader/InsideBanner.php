<section class="InsideBanner">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="BannerText">
                    <h1>create. uplift. transcend.</h1>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="BannerImg">
                    <img src="assets/img/aboutbanner.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>