<section class="InsideBannerWithContent HealOverflowBanner">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="BannerText">
                    <h1>experience</h1>
                    <p>The deep healing power of crystals with certified crystal healer Shiri Perera.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="OverflowBannerImg">
    <div class="ImgBlock">
        <img src="assets/img/heal-banner-img.png" alt="">
    </div>
    </div>
</section>