<?php @include('template-parts/header.php') ?>

<section class="InsideBannerWithContent ImageAtBottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="BannerText">
                    <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ut vulputate nulla. Sed ac velit urna. Curabitur in convallis leo. Vestibulum dignissim metus eros, sed faucibus nisl vulputate non.</h1>
                </div>
            </div>
            <div class="col-12 col-md-5">
                <div class="BannerImg">
                    <img src="assets/img/Image35.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include('template-parts/GreyBgImgWithContent.php')?>

<!-- <section class="Section GroupSession">
    <div class="container">

    </div>
</section> -->

<section class="Section SortingProductLists GroupSessions">
    <div class="container">
        <div class="HeadingWithFilter">
            <div class="HeadingWithResuts">
                <h4 class="BaseColorText">the crystal chooses you</h4>
            </div>
            <div class="ProductSortingDropdown">
                <select class="filters-select">
                    <option value="*">All Dates</option>
                    <option value=".metal,.transition">july 2020</option>
                    <option value=".transition">september 2020</option>
                    <option value=".alkali, .alkaline-earth">november 2020</option>
                </select>
            </div>
        </div>
        <div class="ShowingProducts">
            <div class="row grid">
                <div class="col-12 col-md-12 element-item post-transition metal " data-category="post-transition">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>14<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item post-transition metal" data-category="metal">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>21<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item transition metal" data-category="transition">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>26<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item alkali metal " data-category="alkali">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>14<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12 element-item post-transition metal" data-category="metal">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>22<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item alkali metal " data-category="alkali">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>14<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item transition metal" data-category="transition">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>15<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 element-item post-transition metal " data-category="post-transition">
                    <div class="SessionDate">
                        <div class="DateContent">
                            <div class="Date">
                                <h3>16<br><span>Dec 2020</span></h3>
                            </div>
                            <div class="Content">
                                <h4>Lorem ipsum dolor</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <p><img src="assets/img/date.svg" alt="">Saturday, 14 December 2020, 10:00 am to 12:00 pm | <span>3 hrs</span></p>
                                <p><img src="assets/img/location.svg" alt="">Location: Nilaya House, Global Online</p>
                                <p><img src="assets/img/price.svg" alt="">$200</p>
                            </div>
                        </div>
                        <div class="KnowMore">
                            <a href="" class="BaseBigButton">Know More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section ThemeBaseBg GetInTouchFormBlock">
    <div class="container">
        <h2 class="WhiteText TextCenter">lorem ipsum dolor sit amet,</h2>
        <div class="LeftImgRightForm">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="LeftImg">
                        <img src="assets/img/Group 223.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="RightForm">
                        <h4 class="WhiteText">Lorem ipsum dolor sit amet, consectetur elit.</h4>
                        <form action="">
                            <label>First Name</label>
                            <input type="text" placeholder="Enter your first name">
                            <label>Last Name</label>
                            <input type="text" placeholder="Enter your last name">
                            <label>Company</label>
                            <input type="text" placeholder="Enter your company">
                            <label>Country</label>
                            <input type="text" placeholder="Enter your country">
                            <label>Number of participants</label>
                            <input type="text" placeholder="Enter your participants">
                            <input type="submit" value="Get In Touch">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>