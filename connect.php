<?php @include('template-parts/header.php') ?>

<section class="ConnectBanner">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-7">
                <div class="BannerText">
                    <h1>it would be an honour to connect with you.</h1>
                </div>
            </div>
            <div class="col-12 col-md-5">
                
            </div>
        </div>
    </div>
    <div class="FloatingRightimg">
        <img src="assets/img/tempimg/connectbanner.png" alt="">
    </div>
</section>

<section class="Section ConnectLeftRightContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div class="LeftContent">
                    <p>Aatmaya thrives on fulfilling your needs and intends to build wonderful connections based on sincerity, love and trust.</p>
                    <p>Connect with Aatmaya and you will receive a response to your enquiry within 48 hours.</p>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="RightContent">
                    <ul class="EmailContent">
                        <li>customer service <br><span><a href="mailto:crystalwarriors@aatmaya.com">crystalwarriors@aatmaya.com</a></span></li>
                        <li>enquire <br><span><a href="mailto:info@aatmaya.com">info@aatmaya.com</a></span></li>
                        <li>support <br><span><a href="mailto:admin@aatmaya.com">admin@aatmaya.com</a></span></li>
                    </ul>
                    <div class="SocialBlock">
						<ul>
                            <li><a href="https://www.instagram.com/myaatmaya/" target="_blank"><img src="assets/img/SocialIcons/Insta.svg" alt=""></a></li>
                            <li><a href="https://www.facebook.com/aatmaya" target="_blank"><img src="assets/img/SocialIcons/Facebook.svg" alt=""></a></li>
                            <li><a href="https://www.youtube.com/channel/UCUXLsDrQ7rIxvokvbclYBiQ" target="_blank"><img src="assets/img/SocialIcons/Youtube.svg" alt=""></a></li>
                            <li><a href="https://www.pinterest.com/aatmaya/" target="_blank"><img src="assets/img/SocialIcons/Pinterest.svg" alt=""></a></li>
						</ul>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section ContainerRightImgLeftContent OffWhiteSection">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/connect.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="LeftContent">
                    <h2 class="BaseColorText">custom orders</h2>
                    <p>Aatmaya crystal jewellery is crafted with graceful and delicate precision and holds designs which represent continuous progression, evolution and growth.</p>
                    <p>For custom orders, please fill out the request form below. You will receive a response within 48 hours.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 DesktopOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/connect.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section TestinomialSliders">
    <div class="container">
        <div class="slider">
            <div class="SliderContent">
                <h4>I would honestly recommend everything about Aatmaya; great sense of grace and style, the intricate and elaborate work behind the pendants, how all crystals are carefully chosen and all the deep thought and care that are poured into the work! I am in love with my Turquoise and Amazonite pendant, the green color compliments the golden chain so beautifully and no matter how much I LOVE to wear it on the daily, I usually let it decide when it wants to be worn; it normally happens on days when I feel exceptionally fiery.</h4>
                <h5>Rana Accawi<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased a sublime amethyst ring and a beautiful rose quartz and smokey quartz pendant that I immediately fell in love with as soon as I saw them. What I particularly admire about them is that the gemstones are big with the best quality possible and the design is extremely delicate. I love wearing Aatmaya and having it in my jewelry collection.</h4>
                <h5>Houda Henniche<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased the Moonstone Pendant from Aatmaya today and I must say, it is a beautiful piece of jewellery. Elegant and tastefully crafted, it is sure going to be my go to necklace and pendant. Wonderful work and splendid customer service. I look forward to doing more business with you.</h4>
                <h5>Shazna Hafeez Careem<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I think mere words are not enough to describe the Crystal Warrior, Shiri Perera, behind Aatmaya. Not only does she dedicate her time to travel the world and find the most beautiful gems, she also inspires healing and love and that is truly felt through her work and highpieces. I love everything about Aatmaya, the passion, love and determination are all qualities that are absolutely inspiring.</h4>
                <h5>Tracy Khoueiry<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I love Aatmaya starting from the founder! Her energy is expressed through her crystals. humanity, transcendence, spiritual consciousness, love, unity, universe, and one-being! thank you Aatmaya for you!</h4>
                <h5>Eva Michelle-Naim<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Thank you Aatmaya for genuinely caring and taking the time to help me pick the right crystal. In love with my Malachite pendant and ready for abundance and transformation!</h4>
                <h5>Nathalie El Jorr<br><span>Lebanon</span></h5>
            </div>
        </div>
    </div>
</section>

<section class="Section RequestFormSection OffWhiteSection">
    <div class="container">
        <h2 class="BaseColorText TextCenter">request</h2>
        <div class="FormSection">
            <form action="">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <label>First Name</label>
                        <input type="text" placeholder="Enter your first name">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Last Name</label>
                        <input type="text" placeholder="Enter your last name">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Email Id</label>
                        <input type="email" placeholder="Enter your email id">
                    </div>
                    <div class="col-12 col-md-6">
                        <label>Phone No.</label>
                        <input type="text" placeholder="Enter your phone no.">
                    </div>
                    <div class="col-12 col-md-12">
                        <label>Comment or Question</label>
                        <input type="text" placeholder="Type your message here">
                    </div>
                    <div class="col-12 col-md-12">
                        <div class="SubmitButton">
                            <input type="submit" value="Send Message">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<?php @include('template-parts/RelatedProductSlider.php') ?>


<?php @include('template-parts/footer.php') ?>