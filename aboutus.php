<?php @include('template-parts/header.php') ?>

<section class="InsideBanner">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-9">
                <div class="BannerText">
                    <h1>create. uplift. transcend.</h1>
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="BannerImg">
                    <img src="assets/img/tempimg/about-us-banner.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section ContainerLeftImgRightContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="LeftImg">
                    <img src="assets/img/tempimg/purpose.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="RightContent">
                    <h2 class="BaseColorText">Purpose</h2>
                    <p>To assist in the global shift by assisting seekers on their journey of self-discovery and deep healing. Aatmaya encourages grace and elegance, and is the embodiment of the deep, sacred and healing energies of the metaphysical world.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ContainerRightImgLeftContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/intention.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="LeftContent">
                    <h2 class="BaseColorText">intention</h2>
                    <p>Embrace a journey of spiritual transformation and abundance. Aatmaya helps seekers step out of the confines of their 3-D reality and into alignment with the highest plane of consciousness.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 DesktopOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/intention.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="left_image_right_content DarkThemeBg">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
              <div class="imgWrap">
                  <img src="assets/img/tempimg/journey.png" alt="journey">
              </div>
            </div>
            <div class="col-md-6">
                <div class="contentWrap mr-5">
                    <h2 class="WhiteText">a soulful journey</h2>
                    <p class="WhiteText">Aatmaya is the story of the body, mind and soul written in the form of jewellery, spiritual tools, home accessories and crystal healing. Unique designs uncommon to the human eye, every piece is a tribute to beauty, individuality, music and dance.</p>
                    <p class="WhiteText">Designed and curated to lift one’s soul into the eternal light. Derived from the Sinhalese term for “soul”, Aatmaya is a representation of both the masculine and feminine energies, heralding the message of organic and holistic living through every product, encouraging you to lead a conscious lifestyle.</p>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="Pt ContainerRightImgLeftContent">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 MobileOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/artist.png" alt="">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="LeftContent">
                    <h2 class="BaseColorText">artist, healer, lover</h2>
                    <p>Shiri Perera, owner and founder of Aatmaya, is a certified crystal healer and self-taught jewellery artist. Armed with a deeper understanding of the metaphysical properties and benefits of crystals, she shares her learnings in the form of energy healing and delicate signature jewellery.</p>
                    <p>Having lived her life across the world in Sri Lanka, United Arab Emirates, Lebanon and Canada, Shiri always found herself drawn to the path of a higher calling. Having experienced cosmic alignment during several mind-expanding moments in her journey, Shiri found herself on a quest for self-awakening. This pursuit for a spiritually empowered lifestyle is how Aatmaya came into being.</p>
                </div>
            </div>
            <div class="col-12 col-md-6 DesktopOnly">
                <div class="RightImg">
                    <img src="assets/img/tempimg/artist.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="Section OffWhiteSection TestinomialSliders">
    <div class="container">
        <div class="slider">
            <div class="SliderContent">
                <h4>I would honestly recommend everything about Aatmaya; great sense of grace and style, the intricate and elaborate work behind the pendants, how all crystals are carefully chosen and all the deep thought and care that are poured into the work! I am in love with my Turquoise and Amazonite pendant, the green color compliments the golden chain so beautifully and no matter how much I LOVE to wear it on the daily, I usually let it decide when it wants to be worn; it normally happens on days when I feel exceptionally fiery.</h4>
                <h5>Rana Accawi<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased a sublime amethyst ring and a beautiful rose quartz and smokey quartz pendant that I immediately fell in love with as soon as I saw them. What I particularly admire about them is that the gemstones are big with the best quality possible and the design is extremely delicate. I love wearing Aatmaya and having it in my jewelry collection.</h4>
                <h5>Houda Henniche<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased the Moonstone Pendant from Aatmaya today and I must say, it is a beautiful piece of jewellery. Elegant and tastefully crafted, it is sure going to be my go to necklace and pendant. Wonderful work and splendid customer service. I look forward to doing more business with you.</h4>
                <h5>Shazna Hafeez Careem<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I think mere words are not enough to describe the Crystal Warrior, Shiri Perera, behind Aatmaya. Not only does she dedicate her time to travel the world and find the most beautiful gems, she also inspires healing and love and that is truly felt through her work and highpieces. I love everything about Aatmaya, the passion, love and determination are all qualities that are absolutely inspiring.</h4>
                <h5>Tracy Khoueiry<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I love Aatmaya starting from the founder! Her energy is expressed through her crystals. humanity, transcendence, spiritual consciousness, love, unity, universe, and one-being! thank you Aatmaya for you!</h4>
                <h5>Eva Michelle-Naim<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Thank you Aatmaya for genuinely caring and taking the time to help me pick the right crystal. In love with my Malachite pendant and ready for abundance and transformation!</h4>
                <h5>Nathalie El Jorr<br><span>Lebanon</span></h5>
            </div>
        </div>
    </div>
</section>

<?php @include('template-parts/footer.php') ?>