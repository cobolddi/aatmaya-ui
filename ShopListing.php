<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageheader/NoBanner.php') ?>

<section class="Section SortingProductLists">
    <div class="container">
        <div class="HeadingWithFilter">
            <div class="HeadingWithResuts">
                <h2 class="BaseColorText">the crystal chooses you</h2>
                <p>Showing 1–12 of 12 results</p>
            </div>
            <div class="ProductSortingDropdown">
                <select class="filters-select">
                    <option>Sort By</option>
                    <option>pendants</option>
                    <option >earrings</option>
                    <option >crystal crowns</option>
                </select>
            </div>
        </div>
        <div class="ShowingProducts">
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="SingleProduct.php">
                            <div class="ProductCard">
                                <img src="assets/img/products/Veritas.jpg" alt="">
                                <h5>Veritas <br><span>$ 162.06</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Freya.jpg" alt="">
                                <h5>Freya <br><span>$ 85.44</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Sarah.jpg" alt="">
                                <h5>Sarah <br><span>$ 67.52</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Michale.jpg" alt="">
                                <h5>Michael <br><span>$ 68.71</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/TranQuillitas.jpg" alt="">
                                <h5>Tranquilitas <br><span>$ 115.96</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Sekhmet.jpg" alt="">
                                <h5>Sekhmet <br><span>$ 84.25</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Uriel.jpg" alt="">
                                <h5>Uriel <br><span>$ 112.78</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Satya <br><span>$ 84.46305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/products/Atlantis.jpg" alt="">
                                <h5>Atlantis <br><span>$ 68.71</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Sophie <br><span>$ 84.46305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Lilith <br><span>$ 68.71305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Iris <br><span>$ 158.6025</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Hecate <br><span>$ 154.2765</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Flora <br><span>$ 157.521</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants" data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Asteria <br><span>$ 158.3862</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item crystalcrowns pendants " data-category="crystalcrowns">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Aphrodite <br><span>$ 154.7091</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants " data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Rati <br><span>$ 146.706</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Soteria <br><span>$ 65.56305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Athena <br><span>$ 149.40975</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Laetitia <br><span>$ 185.40879</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants" data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Dione <br><span>$ 193.347</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item crystalcrowns pendants " data-category="crystalcrowns">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Flaunta <br><span>$ 68.71305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants " data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Gabriel <br><span>$ 108.17625</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Metatron <br><span>$ 120.07275</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item crystalcrowns pendants " data-category="crystalcrowns">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Phoebe <br><span>$ 117.369</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants " data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Ariana <br><span>$ 62.41305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Grace <br><span>$ 152.5461</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Jasmine <br><span>$ 124.9395</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Lyana <br><span>$ 147.7875</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants" data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Rosaline <br><span>$ 154.7091</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item crystalcrowns pendants " data-category="crystalcrowns">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Gaia <br><span>$ 122.7765</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants " data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Ariel <br><span>$ 119.20755</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Odette <br><span>$ 218.7906</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Sarah <br><span>$ 150.70755</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Aurora <br><span>$ 65.56305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants" data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Agatha <br><span>$ 84.46305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item crystalcrowns pendants " data-category="crystalcrowns">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Oceana <br><span>$ 59.26305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item earrings pendants " data-category="earrings">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Desree <br><span>$ 74.79675</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li class="whistlistIcon">
                                    <a>
                                        <img src="assets/img/wishlish-theme.svg" alt="" class="unfilled">
                                        <img src="assets/img/wishlist-fill.svg" alt="" class="filledicon">
                                    </a>
                                </li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-3 element-item post-transition pendants ">
                    <div class="ProductWithOptions">
                        <a href="">
                            <div class="ProductCard">
                                <img src="assets/img/1.png" alt="">
                                <h5>Ebony <br><span>$ 65.56305</span></h5>
                            </div>
                        </a>
                        <div class="OptionList">
                            <ul>
                                <li><a href="shoppingcart.php"><img src="assets/img/cart-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/wishlish-theme.svg" alt=""></a></li>
                                <li><a href="#"><img src="assets/img/view-theme.svg" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>


<?php @include('template-parts/footer.php') ?>