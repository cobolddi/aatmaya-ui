<?php @include('template-parts/header.php') ?>


<?php @include('template-parts/pageheader/InsideBannerWithRightImgContent.php') ?>

<?php @include('template-parts/GiftLeftImgRightContent.php') ?>

<?php @include('template-parts/GiftRightImgLeftContent.php') ?>

<section class="Section OffWhiteSection TestinomialSliders">
    <div class="container">
        <div class="slider">
            <div class="SliderContent">
                <h4>I would honestly recommend everything about Aatmaya; great sense of grace and style, the intricate and elaborate work behind the pendants, how all crystals are carefully chosen and all the deep thought and care that are poured into the work! I am in love with my Turquoise and Amazonite pendant, the green color compliments the golden chain so beautifully and no matter how much I LOVE to wear it on the daily, I usually let it decide when it wants to be worn; it normally happens on days when I feel exceptionally fiery.</h4>
                <h5>Rana Accawi<br><span>USA</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased a sublime amethyst ring and a beautiful rose quartz and smokey quartz pendant that I immediately fell in love with as soon as I saw them. What I particularly admire about them is that the gemstones are big with the best quality possible and the design is extremely delicate. I love wearing Aatmaya and having it in my jewelry collection.</h4>
                <h5>Houda Henniche<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I purchased the Moonstone Pendant from Aatmaya today and I must say, it is a beautiful piece of jewellery. Elegant and tastefully crafted, it is sure going to be my go to necklace and pendant. Wonderful work and splendid customer service. I look forward to doing more business with you.</h4>
                <h5>Shazna Hafeez Careem<br><span>Canada</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I think mere words are not enough to describe the Crystal Warrior, Shiri Perera, behind Aatmaya. Not only does she dedicate her time to travel the world and find the most beautiful gems, she also inspires healing and love and that is truly felt through her work and highpieces. I love everything about Aatmaya, the passion, love and determination are all qualities that are absolutely inspiring.</h4>
                <h5>Tracy Khoueiry<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>I love Aatmaya starting from the founder! Her energy is expressed through her crystals. humanity, transcendence, spiritual consciousness, love, unity, universe, and one-being! thank you Aatmaya for you!</h4>
                <h5>Eva Michelle-Naim<br><span>Lebanon</span></h5>
            </div>
            <div class="SliderContent">
                <h4>Thank you Aatmaya for genuinely caring and taking the time to help me pick the right crystal. In love with my Malachite pendant and ready for abundance and transformation!</h4>
                <h5>Nathalie El Jorr<br><span>Lebanon</span></h5>
            </div>
        </div>
    </div>
</section>


<section class="Section RelatedProductSlider">
    <div class="container">
        <h2 class="TextCenter BaseColorText">You may also like</h2>
        <div class="RelatedProduct">
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
            <div class="ProductDetails">
                <a href="">
                    <div class="ProductCard">
                        <img src="assets/img/1.png" alt="">
                        <h5>Agate Way to Hell <br><span>$ 250</span></h5>
                    </div>
                </a>
            </div>
        </div>
        
    </div>
</section>


<?php @include('template-parts/footer.php') ?>